# encoding: utf-8
#MEDIA_DIR, é a variavel que esta contido o caminho do projeto, encontrando a pasta "media"
from django.core.exceptions import ObjectDoesNotExist
import os
import re
import codecs
MEDIA_DIR = os.path.join(os.path.realpath(os.path.dirname("check")), 'check/media')

from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


#Cada class aqui, representa uma tabela no banco de dados

class UnidadeNegocio(models.Model):
    """
    Classe que representa as Unidades de Negócio
    """
    nome = models.CharField(max_length=100)

    def __unicode__(self):
        return "%s" %(self.nome)

class Cargo(models.Model):
    """
    Classe que representa os Cargos
    """

    nome = models.CharField(max_length=50)

    def __unicode__(self):
        return self.nome

class Colaborador(models.Model):
    """
    Classe que representa os Colaboradores
    """
    user = models.OneToOneField(User)

    nome = models.CharField(max_length=150)
    telefone = models.CharField(max_length=36)
    unidade = models.ForeignKey(UnidadeNegocio)
    cargo = models.ForeignKey(Cargo)

    def __unicode__(self):
        return "%s - %s"%(self.unidade, self.nome)

class Empresa(models.Model):
    """
    Classe que representa as Empresas
    """
    #user = models.OneToOneField(User)

    cnpj = models.CharField(max_length=50)
    cod_questor = models.CharField(max_length=50)
    razao_social = models.CharField(max_length=150)
    unidade_negocio = models.ForeignKey(UnidadeNegocio)
    nome_contato = models.CharField(max_length=170)
    telefone_contato = models.CharField(max_length=50)
    colaborador = models.ManyToManyField(Colaborador, through='Colaborador_Empresa')

    def natural_key(self):
        return (self.razao_social)


    def __unicode__(self):
        return u"%s" %(self.razao_social)

class Colaborador_Empresa(models.Model):
    """
    Classe que representa a o relacionamento entre os Colaboradores 
    e as Empresas.
    Cada Colaborador será responsável por algumas empresas
    e as Empresas poderam ter mais de 1 Colaborador responsável
    """
    colaborador = models.ForeignKey(Colaborador)
    empresa = models.ForeignKey(Empresa)

    def __unicode__(self):
        return "%s" %(self.empresa)

class Layout(models.Model):
    """
    Classe que representa os Layout
    """
    nome = models.CharField(max_length=15)
    representacao = models.CharField(max_length=50)

    def natural_key(self):
        return (self.representacao)

    def __unicode__(self):
        return self.representacao

def tipo_arquivo(filex):
    """
    Verifica a extensao do arquivo enviado para o sistema
    """
    SUPORTADOS = ["csv", "txt", "xml", "xls"]

    name = str(filex)
    name_split = name.split(".")
    if not name_split[-1] in SUPORTADOS:
        raise ValidationError("Arquivo do tipo '%s' não é suportado." %(name_split[-1]))

class Document(models.Model):
    """
    Classe que representa os Documentos.
    Documentos importados pelos Colaboradores, cada documento 
    pertencerá a uma Empresa
    """
    VALIDO = 'V'
    ERROS = 'E'
    N_VALIDADO = 'N'
    STATS_CHOICES = (
            (VALIDO,'Validado'),
            (ERROS,'Com Erros'),
            (N_VALIDADO,'Nao Validado')
            )
    docfile = models.FileField(upload_to="recebidos/", validators=[tipo_arquivo])
    empresa = models.ForeignKey(Empresa)
    layout = models.ForeignKey(Layout)
    status = models.CharField(max_length=15, 
            choices=STATS_CHOICES,default=N_VALIDADO)
    data_envio = models.DateTimeField(auto_now_add=True)

    def natural_key(self):
        return '%s/%s' %(self.empresa, self.layout)

    @classmethod
    def get_content(self, file_id, errors_dict = None):
        """
        Return the content file as Lists
        Retorna o conteudo do arquivo como lista.
        """
        get_file = self.objects.get(pk=file_id)
        instance_file = codecs.open(get_file.docfile.path, 'r','iso8859-1')

        if not errors_dict is None:
            content = ((num_line + 1, line.split(";"), erros) for num_line,
                       line in enumerate(instance_file.readlines())\
                       for num_err,erros in errors_dict.items()\
                       if num_line == num_err)
            instance_file.close()
            return content
        else:
            content = ((num_line +1,line.split(";"))\
                    for num_line,line in enumerate(instance_file.readlines()))
            instance_file.close()
            return content

    def __unicode__(self):
        return "%s - %s" %(self.empresa, self.status)

class DeParaDocumentos(models.Model):
    """
    Classe que representa os Documentos que serao salvos apos a realizacao
    do DE-PARA
    """
    EXPORTADO = 'E'
    NAO_EXPORTADO = 'N'
    STATUS_CHOICES = (
            (EXPORTADO, 'Exportado'),
            (NAO_EXPORTADO, 'Nao Exportado'),
            )
    documento = models.FileField(upload_to="validados/")
    documento_origen = models.ForeignKey(Document)
    status = models.CharField(max_length=20,
            choices=STATUS_CHOICES, default=NAO_EXPORTADO)
    data_envio = models.DateTimeField(auto_now_add=True)
    #empresa = models.ForeignKey(Empresa)

    def natural_key(self):
        return (self.documento_origen)

    def __unicode__(self):
        return "%s" %(self.documento)

    @classmethod
    def get_content(self, file_id):
        """ 
            Return the content file as Lists 
            Retorna o conteudo do arquivo como lista.
        """
        get_file = self.objects.get(pk=file_id)
        instance_file = codecs.open(get_file.documento.path,
                'r', 'iso8859-1')

        content = ((num_line+1,line.split(";")) for num_line,line in
                   enumerate(instance_file.readlines()))
        instance_file.close()

        return content

class Padrao(models.Model):
    """
    Classe que representa os Padroes.
    Os padroes são as sequencias que cada layout terá.
    será salvo os 'formatos' como REGEX e também será
    salvo a ordem desse REGEX cadastrado
    """
    formato = models.CharField(max_length=200)
    ordem = models.IntegerField()
    layout = models.ManyToManyField(Layout, through='Layout_Padrao')

    def __unicode__(self):
        return self.formato

class Layout_Padrao(models.Model):
    """
    Classe que representa o relacionamento entre a
    classe Layout e Padrao.
    Os Padroes terão, em vários Layouts, ordens diferentes
    """
    layout = models.ForeignKey(Layout)
    padrao = models.ForeignKey(Padrao)

    def __unicode__(self):
        return "%s" %(self.padrao)

class ContasLancaCont(models.Model):
    """
    Classe que representa as Contas Lançamentos Contabil
    Faz parte dos DE-PARAS
    """
    de = models.CharField(max_length=40)
    para = models.CharField(max_length=40)
    empresa = models.ForeignKey(Empresa)

    def __unicode__(self):
        return "%s" %(self.empresa)

class HistoricoLancaCont(models.Model):
    """
    Classe que representa os Historicos Lançamentos Contabil
    Faz parte dos DE-PARAS
    """
    de = models.CharField(max_length=40)
    para = models.CharField(max_length=40)
    empresa = models.ForeignKey(Empresa)
    def __unicode__(self):
        return "%s" %(self.de)

class ImpostoDePara(models.Model):
    ESTADUAL = 'E'
    MUNICIPAL = 'M'
    FEDERAL = 'F'
    TIPO_IMPOSTO = (
            (ESTADUAL, 'Estadual'),
            (MUNICIPAL, 'Municipal'),
            (FEDERAL, 'Federal'),
            )
    banco = models.CharField(max_length=100)
    conta = models.CharField(max_length=100)
    agencia = models.CharField(max_length=100)
    tb = models.CharField(max_length=20, verbose_name="Tabela Contábil")
    empresa = models.ForeignKey(Empresa)
    tipo = models.CharField(max_length=5, choices=TIPO_IMPOSTO)
    imposto = models.CharField(max_length=100)

    def __unicode__(self):
        return "%s" %(self.de)

class CodImpostoDP(models.Model):

    RETIDO = 'R'
    NAO_RETIDO = 'N'
    FLAG_RETIDO = (
        (RETIDO, 'Retido'),
        (NAO_RETIDO, 'Não Retido'),
    )
    imposto = models.CharField(max_length=100)
    codigo = models.CharField(max_length=100)
    variacao = models.CharField(max_length=100)
    retido = models.CharField(max_length=6,choices=FLAG_RETIDO,
        default=NAO_RETIDO)
    tb = models.CharField(max_length=100)

class DuplicatasDeParaEntrada(models.Model):
    """
    Classe que representa as Duplicatas de Entrada
    Faz parte dos DE-PARAS
    """
    banco = models.CharField(max_length=100)
    agencia = models.CharField(max_length=100)
    conta = models.CharField(max_length=100)
    tb = models.CharField(max_length=20, verbose_name="Tabela Contábil")
    empresa = models.ForeignKey(Empresa)

    class Meta:
        unique_together = (('banco','agencia','conta','empresa'),)

    def __unicode__(self):
        return "%s, %s - %s" %(self.empresa, self.banco, self.tb)

class DuplicatasDeParaSaida(models.Model):
    """
    Classe que representa as Duplicatas de Saída
    Faz parte dos DE-PARAS
    """
    banco = models.CharField(max_length=100)
    agencia = models.CharField(max_length=100)
    conta = models.CharField(max_length=100)
    tb = models.CharField(max_length=20, verbose_name="Tabela Contábil")
    empresa = models.ForeignKey(Empresa)

    class Meta:
        unique_together = (('banco','agencia','conta','empresa'),)

    def __unicode__(self):
        return "%s, %s - %s" %(self.empresa, self.banco, self.tb)

class DeParaFilial(models.Model):
    de = models.CharField(max_length=100)
    para = models.CharField(max_length=100)
    empresa = models.ForeignKey(Empresa)

    @classmethod
    def depara_filial(self, *args):
        de_arg, empresa_arg = args[0], args[1]

        try:
            result = self.objects.get(de__exact=de_arg, empresa__exact=empresa_arg)
            return result
        except ObjectDoesNotExist, e:
            raise ObjectDoesNotExist("Não foi encontrado De Para Filial: %s", de_arg)


    def __unicode__(self):
        return "%s - %s - %s" %(self.empresa, self.de, self.para)


#####################################################################
############################  SIGNALS  ##############################
#####################################################################


#def create_path(sender, instance, signal=None, **kwargs):
#   """
#    Metodo responsavel para criar as pastas das empresa, assim que estiver a
#    empresa for salva no banco de dados
#    """
#    nome da pasta com o padrão da razao_social da empresa
#    name_path = instance.razao_social
#    generators com todas as pastas dentro da pasta /media/
#    paths = (dic for dic in os.listdir(MEDIA_DIR))
#
#    for each_path in paths:
#       #Verifica se as pastas listadas são pastas e
#       # exclui da lista a pasta oculta .DS_Store
#       if not os.path.isdir(each_path) and not each_path == ".DS_Store" :
#           #Lança uma Excpetion, se existir pasta Cadastrada
#           #Mesmo que o Try Catch do Java
#           try:
#              os.mkdir(MEDIA_DIR+"/"+each_path+"/"+name_path)
#           except OSError:
#              print "Já existe está empresa Cadastrada"
#
#
# Metodo responsavel para executar "create_path" e salva a empresa, ele passa
# como argumento, a ultima instancia valida da class Empresa

#models.signals.pre_save.connect(create_path, sender=Empresa)
