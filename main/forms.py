# encoding: utf-8
from main.models import ContasLancaCont, HistoricoLancaCont,DuplicatasDeParaEntrada, DuplicatasDeParaSaida, ImpostoDePara, Document,DeParaFilial
from django import forms
from django.forms import ModelForm

class DocumentForm(ModelForm):

    class Meta:
        model = Document
        exclude = ('status')

class ContaContabForm(ModelForm):

    class Meta:
        model = ContasLancaCont
        exclude = ('empresa')

class HistoricoLancaForm(ModelForm):

    class Meta:
        model = HistoricoLancaCont
        exclude = ('empresa')

class DuplicatasDeParaEntradaForm(ModelForm):

    class Meta:
        model = DuplicatasDeParaEntrada
        exclude = ('empresa')

class DuplicatasDeParaSaidaForm(ModelForm):

    class Meta:
        model = DuplicatasDeParaSaida
        exclude = ('empresa')

class ImpostoDeParaForm(ModelForm):

    class Meta:
        model = ImpostoDePara
        exclude = ('empresa')

class DeParaFilialForm(ModelForm):

    class Meta:
        model = DeParaFilial
        exclude = ('empresa')
