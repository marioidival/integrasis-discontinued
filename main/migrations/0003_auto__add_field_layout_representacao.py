# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Layout.representacao'
        db.add_column(u'main_layout', 'representacao',
                      self.gf('django.db.models.fields.CharField')(default=' ', max_length=50),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Layout.representacao'
        db.delete_column(u'main_layout', 'representacao')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'main.cargo': {
            'Meta': {'object_name': 'Cargo'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome_unidade': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'main.colaborador': {
            'Meta': {'object_name': 'Colaborador'},
            'cargo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Cargo']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'telefone': ('django.db.models.fields.CharField', [], {'max_length': '36'}),
            'unidade': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.UnidadeNegocio']"}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'main.colaborador_empresa': {
            'Meta': {'object_name': 'Colaborador_Empresa'},
            'colaborador': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Colaborador']"}),
            'empresa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Empresa']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'main.contaslancacont': {
            'Meta': {'object_name': 'ContasLancaCont'},
            'de': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'empresa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Empresa']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'para': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'main.deparadocumentos': {
            'Meta': {'object_name': 'DeParaDocumentos'},
            'data_envio': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'documento': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'documento_origen': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Document']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'N'", 'max_length': '20'})
        },
        u'main.deparafilial': {
            'Meta': {'object_name': 'DeParaFilial'},
            'de': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'empresa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Empresa']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'para': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'main.document': {
            'Meta': {'object_name': 'Document'},
            'data_envio': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'docfile': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'empresa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Empresa']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'layout': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Layout']"}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'N'", 'max_length': '15'})
        },
        u'main.duplicatasdeparaentrada': {
            'Meta': {'object_name': 'DuplicatasDeParaEntrada'},
            'agencia': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'banco': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'conta': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'empresa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Empresa']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tb': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'main.duplicatasdeparasaida': {
            'Meta': {'object_name': 'DuplicatasDeParaSaida'},
            'agencia': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'banco': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'conta': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'empresa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Empresa']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tb': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'main.empresa': {
            'Meta': {'object_name': 'Empresa'},
            'cnpj': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'cod_questor': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'colaborador': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['main.Colaborador']", 'through': u"orm['main.Colaborador_Empresa']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome_contato': ('django.db.models.fields.CharField', [], {'max_length': '170'}),
            'razao_social': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'telefone_contato': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'unidade_negocio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.UnidadeNegocio']"})
        },
        u'main.historicolancacont': {
            'Meta': {'object_name': 'HistoricoLancaCont'},
            'de': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'empresa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Empresa']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'para': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'main.impostodepara': {
            'Meta': {'object_name': 'ImpostoDePara'},
            'agencia': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'banco': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'conta': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'empresa': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Empresa']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tb': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'main.layout': {
            'Meta': {'object_name': 'Layout'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'representacao': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'main.layout_padrao': {
            'Meta': {'object_name': 'Layout_Padrao'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'layout': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Layout']"}),
            'padrao': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Padrao']"})
        },
        u'main.padrao': {
            'Meta': {'object_name': 'Padrao'},
            'formato': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'layout': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['main.Layout']", 'through': u"orm['main.Layout_Padrao']", 'symmetrical': 'False'}),
            'ordem': ('django.db.models.fields.IntegerField', [], {})
        },
        u'main.unidadenegocio': {
            'Meta': {'object_name': 'UnidadeNegocio'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['main']