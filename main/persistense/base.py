# encoding: utf-8
import fdb

class ConexaoQuestor(object):

    def __init__(self, empresa, nota, fornecedor):
        self.empresa = empresa
        if nota.find('/') == -1:
            self.nota = int(nota)
        else:
            self.nota = nota
        self.fornecedor = fornecedor
        self.connection = fdb.connect(
                host='192.168.168.15', database='/opt/Questor/questor.fdb',
                user='EDI', password='c5v#L5@XHM'
                )

    def busca_codigo_fornecedor(self):
        """
        Buscar Codigo do Fornecedor para usar nos outros metodos
        Retorna [CODIGOPESSOA]
        Busca no QUESTOR o codigo do fornecedor pelo CNPJ
        Valor retornado add no final do arquivo gerado 'de-para'
        """

        con = self.connection
        cur = con.cursor()

        consulta = "SELECT codigopessoa FROM pessoa WHERE inscrfederal='%s'"\
                %(self.fornecedor)
        try:
            cur.execute(consulta)
        except Exception:
            raise Exception("Erro ao realizar consulta: "+self.fornecedor)
        else:
            try:
                result = cur.fetchone()
                codigo_fornecedor = result[0]
            except Exception:
                raise Exception("Codigo do Fornecedor:"\
                                +self.fornecedor+ "Nao encontrado")
            else:
                yield codigo_fornecedor

    def buscar_duplicata(self):
        """
        Retorna [CHAVELCTOFISENT]
        No Java, recebe empresa, nota, fornecedor
        'fornecedor'-> cpf_cnpj (busca_codigo_fornecedor)
        'empresa' -> busca no banco do IntegraSis
        'nota' -> pega no arquivo
        Valor retornado add no final do arquivo gerado 'de-para'
        """

        cpf_cnpj = self.busca_codigo_fornecedor()
        con = self.connection
        cur = con.cursor()

        consulta = "SELECT chaveduplicataent from duplicataent WHERE codigoempresa="\
                "%s and codigopessoa=%s and numeroduplicataent=%s"\
                %(self.empresa,cpf_cnpj.next(), str(self.nota))
        try:
            cur.execute(consulta)
        except Exception:
            raise Exception("Erro ao realizar consulta: "+str(self.nota))
        else:
            try:
                result = cur.fetchone()
                chave = str(result[0])
            except Exception:
                raise Exception("Nenhuma Chave Duplicata da nota foi encontrada: "\
                                + str(self.nota))
            else:
                yield chave

    def buscar_seq_pagamento(self):
        """
        empresa -> busca no banco do integrasis
        chave -> resultado da funcao buscar_duplicata
        cpf_cnpj -> resultado da funcao buscar_codigo_fornecedor
        Retorna a SEQ no pagamento
        O padrao da SEQ e 0, quando for realizada a busca add +1 a SEQ
        mesmo retornando Null da consulta
        """
        con = self.connection
        cur = con.cursor()

        sequencia = 0

        try:
            cpf_cnpj = self.busca_codigo_fornecedor()
        except Exception, e:
            raise Exception("A busca da sequencia necessita do codigo do"\
                            " fornecedor valido")

        try:
            chave = self.buscar_duplicata()
        except Exception, e:
            raise Exception("A busca da sequencia necessita da chave"\
                            " duplicata valida")

        consulta = "SELECT seq FROM duplicataentpgto WHERE codigoempresa=%s and "\
                "chaveduplicataent=%s and codigopessoa=%s order by seq desc"\
                %(self.empresa, chave.next(), cpf_cnpj.next())
        try:
            cur.execute(consulta)
        except Exception, e:
            raise Exception("Erro em buscar Sequencia")
        else:
            result = cur.fetchone()

            if result is None:
                sequencia += 1
                return sequencia
            else:
                sequencia = int(str(result[0]))
                sequencia += 1
                return sequencia

    def buscar_duplicata_sai(self):
        cpf_cnpj = self.busca_codigo_fornecedor()
        con = self.connection
        cur = con.cursor()

        consulta = "SELECT chaveduplicatasai from duplicatasai WHERE codigoempresa="\
                "%s and codigopessoa=%s and numeroduplicatasai=%s"\
                %(self.empresa,cpf_cnpj.next(), str(self.nota))
        try:
            cur.execute(consulta)
        except Exception:
            raise Exception("Erro ao realizar consulta: "+str(self.nota))
        else:
            try:
                result = cur.fetchone()
                chave = str(result[0])
            except Exception:
                raise Exception("Nenhuma Chave Duplicata da nota foi encontrada: "\
                                + str(self.nota))
            else:
                return chave

    def buscar_seq_pagamento_sai(self):
        con = self.connection
        cur = con.cursor()

        sequencia = 0

        try:
            chave = self.buscar_duplicata_sai()
        except Exception, e:
            raise Exception("A busca da sequencia necessita da chave"\
                            " duplicata valida")

        consulta = "SELECT seq FROM duplicatasaipgto WHERE codigoempresa=%s and "\
                "chaveduplicatasai=%s order by seq desc"\
                %(self.empresa, chave)
        try:
            cur.execute(consulta)
        except Exception, e:
            raise Exception("Erro em buscar Sequencia")
        else:
            result = cur.fetchone()

            if result is None:
                sequencia += 1
                return sequencia
            else:
                sequencia = int(str(result[0]))
                sequencia += 1
                return sequencia

    def __done__(self):
        self.connection.close()


class ChaveImposto(object):

    def __init__(self, empresa, data, cod_imposto=None, variacao=None,
        fornecedor=None, retido=None):
        form_data = data.replace('/','.')
        self.empresa = empresa
        self.data = form_data
        self.cod_imposto = cod_imposto
        self.variacao = variacao
        self.fornecedor = fornecedor
        self.retido = retido
        self.connection = fdb.connect(
                host='192.168.168.15', database='/opt/Questor/questor.fdb',
                user='EDI', password='c5v#L5@XHM'
                )

    def chave_municipal(self):
        con = self.connection
        cur = con.cursor()

        consulta = "select chavedebitomun from debitomunicipal where"\
                   " datalctoctr='%s' and codigoimposto = 9986 and "\
                   "variacaoimposto=0 and codigoempresa=%s"\
                   %(self.data, self.empresa)
        try:
            cur.execute(consulta)
        except Exception:
            raise Exception("Erro ao realizar consulta")
        else:
            try:
                result = cur.fetchone()
                chv_mun = result[0]
            except Exception:
                raise Exception("Nenhuma Chave Municipal foi encontrada")
            else:
                yield chv_mun

    def chave_estadual(self):
        con = self.connection
        cur = con.cursor() 

        consulta = "select chavedebitoest from debitoestadual where"\
                   " datalctoctr='%s' and codigoimposto=%s and "\
                   "variacaoimposto=%s and codigoempresa=%s"\
                   %(self.data, self.cod_imposto, self.variacao, self.empresa)
        try:
            cur.execute(consulta)
        except Exception:
            raise Exception("Erro ao realizar consulta")
        else:
            try:
                result = cur.fetchone()
                chv_mun = result[0]
            except Exception:
                raise Exception("Nenhuma Chave Estadual foi encontrada")
            else:
                yield chv_est

    def chave_federal_n_retido(self):
        con = self.connection
        cur = con.cursor() 

        consulta = "select chavedebitofed from debitofederal where"\
                   " datalctoctr='%s' and codigoimposto='%s' and "\
                   "variacaoimposto='%s' and codigoempresa='%s'"\
                   %(self.data, self.cod_imposto, self.variacao, self.empresa)
        try:
            cur.execute(consulta)
        except Exception:
            raise Exception("Erro ao realizar consulta")
        else:
            try:
                result = cur.fetchone()
                chv_n_fed = result[0]
            except Exception:
                raise Exception("Nenhuma Chave Federal foi encontrada")
            else:
                return chv_n_fed

    def chave_federal_retido(self):
        con = self.connection
        cur = con.cursor() 

        consulta = "select chavedebitofed from debitofederal where"\
                   " datalctoctr='%s' and codigoimposto = '%s' and "\
                   "variacaoimposto = '%s' and codigoempresa = '%s' "\
                   "and OBSERVDEBITO like '%s'"\
                   %(
                        self.data, self.cod_imposto, self.variacao, self.empresa,
                        self.fornecedor
                    )
        try:
            cur.execute(consulta)
        except Exception:
            raise Exception("Erro ao realizar consulta")
        else:
            try:
                result = cur.fetchone()
                chv_mun = result[0]
            except Exception:
                raise Exception("Nenhuma Chave Federal foi encontrada")
            else:
                return chv_n_fed

    def seq_municipal(self):
        con = self.connection
        cur = con.cursor()

        sequencia = 0

        try:
            chave = self.chave_municipal()
        except Exception, e:
            raise Exception("A busca da sequencia necessita da Chave"\
                            " Municipal valida")

        consulta = "SELECT first 1 seq from creditomunicipal where chavedebitomun=%s and "\
                "codigoempresa=%s order by seq desc"\
                %(chave.next(), self.empresa)
        try:
            cur.execute(consulta)
        except Exception, e:
            raise Exception("Erro em buscar Sequencia")
        else:
            result = cur.fetchone()

            if result is None:
                sequencia += 1
                return sequencia
            else:
                sequencia = int(str(result[0]))
                sequencia += 1
                return sequencia

    def seq_estadual(self):
        con = self.connection
        cur = con.cursor()

        sequencia = 0

        try:
            chave = self.chave_estadual()
        except Exception, e:
            raise Exception("A busca da sequencia necessita da Chave"\
                            " Estadual valida")

        consulta = "SELECT first 1 seq from creditoestadual where chavedebitoest =%s and "\
                "codigoempresa=%s order by seq desc"\
                %(chave.next(), self.empresa)
        try:
            cur.execute(consulta)
        except Exception, e:
            raise Exception("Erro em buscar Sequencia")
        else:
            result = cur.fetchone()

            if result is None:
                sequencia += 1
                return sequencia
            else:
                sequencia = int(str(result[0]))
                sequencia += 1
                return sequencia

    def seq_federal(self):
        con = self.connection
        cur = con.cursor()

        if self.retido == 'R':
            try:
                chave = self.chave_federal_retido()
            except Exception, e:
                raise Exception("Necessita de uma Chave Federal Valida")
        else:
            try:
                chave = self.chave_federal_n_retido()
            except Exception, e:
                raise Exception("Necessita de uma Chave Federal Valida")

        sequencia = 0
        consulta = "SELECT first 1 seq from creditofederal where chavedebitofed=%s and "\
                "codigoempresa=%s order by seq desc"\
                %(chave, self.empresa)
        try:
            cur.execute(consulta)
        except Exception, e:
            raise Exception("Erro em buscar Sequencia")
        else:
            result = cur.fetchone()
            if result is None:
                sequencia += 1
                return sequencia
            else:
                sequencia = int(str(result[0]))
                sequencia += 1
                return sequencia

    def __done__(self):
        self.connection.close()

