from base import ConexaoQuestor

arquivo = open()
dic = {}

for lines in arquivo.readlines():
    l = lines.split(';')

    codigo_filial = 1
    fornecedor = l[1].strip()
    nota = l[2].strip()


    instancia_qust = ConexaoQuestor(codigo_filial, nota, fornecedor)

    duplicata = instancia_qust.buscar_duplicata()
    seq  = instancia_qust.buscar_seq_pagamento()

    dic[nota] = duplicata
    chave = str(nota) + str(fornecedor)
    dic[chave] = seq


