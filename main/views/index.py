# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import RequestContext, loader, Context
from django.http import HttpResponse, HttpResponseRedirect
from django.forms.models import inlineformset_factory
from django.core.urlresolvers import reverse
from django.core import serializers
from django.db.models import Q
from main.validador.validador import validator, de_para
from main.models import (
        Document, Empresa, ContasLancaCont,DeParaDocumentos,\
        HistoricoLancaCont, DuplicatasDeParaEntrada, DuplicatasDeParaSaida,\
        ImpostoDePara, DeParaFilial
)
from main.forms import DocumentForm
from django.contrib import messages

# Decorator responsavel por permitir o acesso ao metodo apenas se o usuario
# estiver logado
from django.contrib.auth.decorators import login_required

import datetime
import operator

TEMPLATE_BY_LAYOUT_DETALHE = {
        '01' : 'arquivos/detalhe_01.html', #lancamento Contabeis
        '02' : 'arquivos/detalhe_02.html', #Duplicatas Entrada
        '03' : 'arquivos/detalhe_03.html', #Duplicatas Saida
        '04' : 'arquivos/detalhe_04.html', #imposto municipal
        '05' : 'arquivos/detalhe_05.html', #imposto estadual
        '06' : 'arquivos/detalhe_06.html', #imposto federal
}
TEMPLATE_BY_LAYOUT_VALIDACAO = {
        '01' : 'arquivos/validado_01.html', #Lancamento Contabeis
        '02' : 'arquivos/validado_02.html', #Duplicatas Entrada
        '03' : 'arquivos/validado_03.html', #Duplicatas Saida
        '04' : 'arquivos/validado_04.html', #Imposto Municipal
        '05' : 'arquivos/validado_05.html', #Imposto Estadual
        '06' : 'arquivos/validado_06.html', #Imposto Federal
}
TEMPLATE_BY_LAYOUT_DE_PARA = {
        '01' : 'deparas/contabil/de_para_contabil.html',
        '02' : 'deparas/fiscal/de_para_fiscal.html',
        '03' : 'deparas/fiscal/de_para_fiscal.html',
        '04' : 'deparas/tributos/de_para_municipal.html',
        '05' : 'deparas/tributos/de_para_estadual.html',
        '06' : 'deparas/tributos/de_para_federal.html',
}
TEMPLATE_BY_LAYOUT_MODULO = {
        '#contabil' : 'deparas/contabil/index.html',
        '#fiscal' : 'deparas/fiscal/index.html',
        '#tributos' : 'deparas/tributos/index.html',
}
TEMPLATE_BY_LAYOUT_DETALHE_DEPARA = {
        '01' : 'arquivos/detalhe_depara_01.html', #lancamento Contabeis
        '02' : 'arquivos/detalhe_depara_02.html', #Duplicatas Entrada
        '03' : 'arquivos/detalhe_depara_03.html', #Duplicatas Saida
        '04' : 'arquivos/detalhe_depara_03.html', #imposto municipal
        '05' : 'arquivos/detalhe_depara_04.html', #imposto estadual
        '06' : 'arquivos/detalhe_depara_05.html', #imposto federal
}

@login_required()
def home(request):
    """ 
    Metodo responsavel por renderizar a pagina inicial
    """
    return render_to_response('index.html', {},
            context_instance=RequestContext(request))

@login_required()
def lista_arquivo(request):
    """
    Metodo repsonsavel por renderizar e listar os arquivos
    na pagina de upload de arquivos
    """

    return render_to_response('arquivos/lista_arquivos.html',{},
            context_instance=RequestContext(request))

@login_required()
def detalhe_arquivo(request, arq):
    """
        Metodo responsavel por renderizar e detalhar arquivos escolhidos
        pelo os usuarios.
        Esse metodo usa outro metodo criado na classe Document
        'get_content(arquivo_id)'
    """
    filex = get_object_or_404(Document, pk=arq)
    data = Document.get_content(arq)

    return render_to_response(TEMPLATE_BY_LAYOUT_DETALHE[filex.layout.nome],
            locals(), context_instance=RequestContext(request))

@login_required()
def valida_arquivo(request, arq):
    """
    Metodo responsavel por renderizar a pagina e validar os arquivos escolhidos
    pelo os usuarios.
    Usa um metodo de um modulo criado para validacao e de-paras 
    'validator(caminho_do_arquivo)'
    """
    filex = get_object_or_404(Document, pk=arq)
    data, tamanho = validator(filex)
    try:
        tamanho_errada = len(tamanho['erradas'])
    except:
        tamanho_errada = 0

    try:
        tamanho_correta = len(tamanho['corretas'])
    except:
        tamanho_correta = 0

    if tamanho_errada > 0:
        filex.status = "E"
        filex.save()
    elif tamanho_errada == 0:
        filex.status = "V"
        filex.save()
    else:
        pass

    return render_to_response(TEMPLATE_BY_LAYOUT_VALIDACAO[filex.layout.nome],
            locals(), context_instance=RequestContext(request))

@login_required()
def use_de_para(request, arq):
    """
    Metodo responsavel por renderizar a pagina e fazer os de-paras dos
    arquivos escolhidos pelo os usuarios.
    usa um metodo de um modulo criado para validacao e de-paras
    'de_para(caminho_do_arquivo)'.
    """
    filex = get_object_or_404(Document, pk=arq)
    if len(de_para(filex)) == 2:
        data_view, nome_arquivo = de_para(filex)
    else:
        data_view = de_para(filex)
        nome_arquivo = None

    if type(data_view) == dict:
        if nome_arquivo:
            try:
                arquivo_depara = get_object_or_404(DeParaDocumentos,
                    documento_origen=filex)
            except:
                obj = DeParaDocumentos.objects.create(documento=nome_arquivo,
                            documento_origen=filex)
                obj.save()
                messages.success(request, "De Para Parcial realizado!")
                data = Document.get_content(arq, data_view)
                return render_to_response(
                        TEMPLATE_BY_LAYOUT_DETALHE_DEPARA[filex.layout.nome], locals(),
                    context_instance=RequestContext(request)
                )
            else:
                arquivo_depara.documento = nome_arquivo
                arquivo_depara.data_envio = datetime.datetime.today()
                arquivo_depara.save()
                messages.success(request, "Documento De Para Parcial atualizado!")
                return redirect(lista_arquivo)
        else:
            data = Document.get_content(arq, data_view)
            return render_to_response(
                    TEMPLATE_BY_LAYOUT_DETALHE_DEPARA[filex.layout.nome], locals(),
                context_instance=RequestContext(request)
            )

    else:
        try:
            arquivo_depara = get_object_or_404(DeParaDocumentos,
                    documento_origen=filex)
        except:
            if data_view:
                obj = DeParaDocumentos.objects.create(documento=data_view,
                        documento_origen=filex)
                obj.save()
                messages.success(request, "De Para realizado com Sucesso!")
                return redirect(lista_arquivo)
            else:
                pass
        else:
            arquivo_depara.documento = data_view
            arquivo_depara.data_envio = datetime.datetime.today()
            arquivo_depara.save()
            messages.success(request, "Documento De Para Atualizado!")
            return redirect(lista_arquivo)

@login_required()
def upload(request):
    """
        Metodo responsavel por renderizar a pagina e fazer o upload dos arquivos
        enviados pelos usuarios.
    """
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)

        if form.is_valid():
            messages.success(request, "Arquivo Salvo com Sucesso")
            form.save()
            return redirect(upload)
        else:
            #import ipdb; ipdb.set_trace()
            pass
    else:
        form = DocumentForm()

    documents = Document.objects.order_by('-data_envio')
    return render_to_response('upload.html',locals(), context_instance=RequestContext(request))

@login_required()
def empresa_por_modulo(request):
    """
        Na pagina inicial do de-para, existe um select populado
        pelos modulos
        Contabil / Fiscal / Tributos
        Esse metodo popula outro select vazio que existe na pagina
        com as empresas que existem em cada modulo
        Retorna um JSON
    """
    modulo = request.GET["modulo"]
    if request.method == "GET":
        data = {}
        if modulo == "#contabil" or modulo == "#fiscal" or modulo == "#tributos":
            data = serializers.serialize("json",list(Empresa.objects.all()))
        else:
            pass #criar mensagem de erro!

    return HttpResponse(data, mimetype="applicaton/json")

@login_required()
def index_depara(request):
    """
    Metodo responsavel por renderizar a pagina inicial do modulos de
    de-paras.
    """
    return render_to_response('deparas/index.html', locals(),
            context_instance=RequestContext(request))

@login_required()
def ajax_index_depara(request):
    """
    Este metodo e responsavel por retornar os deparas da empresa
   selecionada juntamente com o modulo 'de_para_por_empresa'
    Pega o modulo passado juntamemte com a empresa e faz a busca
    depois, retorna a busca, ja com os parametros dentro do template
    """
    modulo = request.POST["modulo"]
    empresa_id = request.POST["empresa_id"]

    if request.method == "POST":
        if modulo == "#contabil":
            contas_contabeis = ContasLancaCont.objects.filter(
                    empresa__exact=empresa_id).order_by('de')
            historicos = HistoricoLancaCont.objects.filter(
                    empresa__exact=empresa_id).order_by('de')
        elif modulo == "#fiscal":
            duplicatas_entradas = DuplicatasDeParaEntrada.objects.filter(
                    empresa__exact=empresa_id).order_by('banco')
            duplicatas_saidas = DuplicatasDeParaSaida.objects.filter(
                    empresa__exact=empresa_id).order_by('banco')
            depara_filiais = DeParaFilial.objects.filter(
                    empresa__exact=empresa_id).order_by('de')
        elif modulo == "#tributos":
            impostos = ImpostoDePara.objects.filter(empresa__exact=empresa_id)
        else:
            pass

    return render_to_response(TEMPLATE_BY_LAYOUT_MODULO[modulo], locals(),
            context_instance=RequestContext(request))

@login_required()
def detalhe_arquivo_depara(request, arq):
    """
    Metodo responsavel por renderizar e detalhar arquivos escolhidos
    pelo os usuarios.
    Esse metodo usa outro metodo criado na classe Document
    'get_content(arquivo_id)'
    """
    filex = get_object_or_404(DeParaDocumentos, pk=arq)
    data = DeParaDocumentos.get_content(arq)

    return render_to_response(
            TEMPLATE_BY_LAYOUT_DE_PARA[filex.documento_origen.layout.nome],
            locals(), context_instance=RequestContext(request))

@login_required()
def download_file(request, arq):
    """
    Method response file for download
    """
    filex = get_object_or_404(DeParaDocumentos, pk=arq)
    filename = filex.documento.name.split('/')[-1]
    response = HttpResponse(filex.documento, content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename

    return response

@login_required()
def adv_search(request):

    empresa_form = request.GET.get('empresa', None)
    nome_doc_form = request.GET.get('nomedoc', None)
    layout_form = request.GET.get('layout', None)
    data_in = request.GET.get('data_in', None)
    data_ou = request.GET.get('data_ou', None)
    status_form = request.GET.get('status_f', None)

    query_list = []
    try:
        data_in_form = format_date(data_in)
    except:
        data_in_form = None
    try:
        data_ou_form = format_date(data_ou)
    except:
        data_ou_form = None

    if empresa_form:
        query = ('empresa', empresa_form)
        query_list.append(query)

    if nome_doc_form:
        query = ('docfile__icontains', nome_doc_form)
        query_list.append(query)

    if layout_form:
        query = ('layout', layout_form)
        query_list.append(query)

    if data_in_form:
        if data_ou:
            query = ('data_envio__gte', data_in_form)
            query2 = ('data_envio__lte', data_ou_form)
            query_list.append(query)
            query_list.append(query2)
        else:
            query = ('data_envio__gte', data_in_form)
            query_list.append(query)

    if status_form:
        query = ('status', status_form)
        query_list.append(query)

    q_list = (Q(x) for x in query_list)
    data = serializers.serialize('json', list(
        Document.objects.select_related().filter(reduce(operator.and_, q_list)).order_by('-data_envio') 
        ), use_natural_keys=True
    )

    return HttpResponse(data, mimetype="applicaton/json")

@login_required()
def adv_search_depara(request):

    empresa_form = request.GET.get('empresa_up', None)
    layout_form = request.GET.get('layout_up', None)
    data_in = request.GET.get('data_in_up', None)
    data_ou = request.GET.get('data_ou_up', None)

    query_list = []
    try:
        data_in_form = format_date(data_in)
    except:
        data_in_form = None
    try:
        data_ou_form = format_date(data_ou)
    except:
        data_ou_form = None

    if empresa_form:
        query = ('documento_origen__empresa_id', empresa_form)
        query_list.append(query)

    if data_in_form:
        if data_ou:
            query = ('data_envio__gte', data_in_form)
            query2 = ('data_envio__lte', data_ou_form)
            query_list.append(query)
            query_list.append(query2)
        else:
            query = ('data_envio__gte', data_in_form)
            query_list.append(query)

    if layout_form:
        query = ('documento_origen__layout_id', layout_form)
        query_list.append(query)

    q_list = [Q(x) for x in query_list]
    data = serializers.serialize('json', list(
        DeParaDocumentos.objects.select_related().filter(reduce(operator.and_, q_list)).order_by('-data_envio')
        ), use_natural_keys=True
    )
    return HttpResponse(data, mimetype="applicaton/json")

@login_required()
def empresas(request):
    '''
    Return all Empresas JSON
    '''

    empresa_id = request.GET.get('id', None)
    if empresa_id:
        data = serializers.serialize('json', list(
            Empresa.objects.filter(id=empresa_id)
            )
        )
    else:
        data = serializers.serialize('json', list(
            Empresa.objects.all()
            )
        )
    return HttpResponse(data, mimetype='applicaton/json')

@login_required()
def documentos(request):
    '''
    RETURN ALL DOCUMENTS JSON
    '''
    data = serializers.serialize('json', list(
        Document.objects.all().order_by('-data_envio')
        ), use_natural_keys=True
    )
    return HttpResponse(data, mimetype='applicaton/json')

@login_required()
def documentos_depara(request):
    '''
    RETURN ALL DOCUMENTS OF DEPARA JSON
    '''
    data = serializers.serialize('json', list(
        DeParaDocumentos.objects.all().order_by('-data_envio')
        ), use_natural_keys=True
    )
    return HttpResponse(data, mimetype='applicaton/json')

def format_date(dates):
    try:
        new_date = dates.split('/')
    except IndexError:
        return None
    else:
        return datetime.datetime(
            int(new_date[2]),int(new_date[1]),int(new_date[0])
        )
