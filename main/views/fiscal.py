# encoding: utf-8
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.forms.models import inlineformset_factory
from django.template import RequestContext
from main.views.index import index_depara
from django.contrib import messages

from main.models import DuplicatasDeParaEntrada,DuplicatasDeParaSaida,Empresa,DeParaFilial
from main.forms import DuplicatasDeParaEntradaForm, DuplicatasDeParaSaidaForm,DeParaFilialForm
from django.contrib.auth.decorators import login_required

@login_required()
def create_fiscal_entrada(request):

    empresa_i = request.GET['empresa_id']

    empresa = Empresa.objects.get(pk=empresa_i)
    DuplicatasFormset = inlineformset_factory(Empresa, DuplicatasDeParaEntrada, max_num=1, can_delete=False)
    formset = DuplicatasFormset(request.POST or None)

    if request.method == "POST":
        if formset.is_valid():
            data = formset.cleaned_data
            if not data == [{}]:

                for toba in data:
                    toba['empresa'] = empresa
                    DuplicatasDeParaEntrada.objects.create(**toba)
                return redirect(index_depara)
            else:
                messages.error(request, "Preencha todos os campos")

    return render_to_response('deparas/fiscal/new.html',locals(),
            context_instance=RequestContext(request))

@login_required()
def edit_fiscal_entrada(request, conta):

    obj = get_object_or_404(DuplicatasDeParaEntrada, pk=conta)
    if request.method == "POST":
        form = DuplicatasDeParaEntradaForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            return redirect(index_depara)
        else:
            return render_to_response('deparas/fiscal/edit.html', locals(),
                    context_instance=RequestContext(request))
    else:
        form = DuplicatasDeParaEntradaForm(instance=obj)

    return render_to_response('deparas/fiscal/edit.html', locals(),
                    context_instance=RequestContext(request))

@login_required()
def delete_fiscal_entrada(request, conta):
    obj = get_object_or_404(DuplicatasDeParaEntrada, pk=conta)
    if obj:
        obj.delete()
    return redirect(index_depara)

#SAIDA
@login_required()
def create_fiscal_saida(request):

    empresa_i = request.GET['empresa_id']

    empresa = Empresa.objects.get(pk=empresa_i)
    DuplicatasFormset = inlineformset_factory(Empresa, DuplicatasDeParaSaida, max_num=1, can_delete=False)
    formset = DuplicatasFormset(request.POST or None)

    if request.method == "POST":
        if formset.is_valid():
            data = formset.cleaned_data
            for toba in data:
                toba['empresa'] = empresa
                DuplicatasDeParaSaida.objects.create(**toba)
            return redirect(index_depara)
        else:
            pass
    return render_to_response('deparas/fiscal/new.html',locals(),
            context_instance=RequestContext(request))

@login_required()
def edit_fiscal_saida(request, conta):

    obj = get_object_or_404(DuplicatasDeParaSaida, pk=conta)
    if request.method == "POST":
        form = DuplicatasDeParaSaidaForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            return redirect(index_depara)
        else:
            return render_to_response('deparas/fiscal/edit.html', locals(),
                    context_instance=RequestContext(request))
    else:
        form = DuplicatasDeParaSaidaForm(instance=obj)

    return render_to_response('deparas/fiscal/edit.html', locals(),
                    context_instance=RequestContext(request))

@login_required()
def delete_fiscal_saida(request, conta):
    obj = get_object_or_404(DuplicatasDeParaSaida, pk=conta)
    if obj:
        obj.delete()
        return redirect(index_depara)
        return redirect(index_depara)

#DE PARA FILIAL
@login_required()
def create_depara_filial(request):
    empresa_i = request.GET['empresa_id']

    empresa = Empresa.objects.get(pk=empresa_i)
    DeParaFilialFormset = inlineformset_factory(Empresa, DeParaFilial,
            max_num=1, can_delete=False)
    formset = DeParaFilialFormset(request.POST or None)

    if request.method == "POST":
        if formset.is_valid():
            data = formset.cleaned_data
            for campos in data:
                campos['empresa'] = empresa
                DeParaFilial.objects.create(**campos)
            return redirect(index_depara)
        else:
            pass
    return render_to_response('deparas/fiscal/new_depara_filial.html',
            locals(), context_instance=RequestContext(request))

@login_required()
def edit_depara_filial(request, depara):
    obj = get_object_or_404(DeParaFilial, pk=depara)
    if request.method == "POST":
        form = DeParaFilialForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            return redirect(index_depara)
        else:
            return render_to_response('deparas/fiscal/edit_depara_filial.html', locals(),
                    context_instance=RequestContext(request))
    else:
        form = DeParaFilialForm(instance=obj) 
    return render_to_response('deparas/fiscal/edit_depara_filial.html',
            locals(), context_instance=RequestContext(request))

@login_required()
def delete_depara_filial(request, depara):
    """docstring for delete_depara_filial"""
    obj = get_object_or_404(DeParaFilial, pk=depara)
    if obj:
        obj.delete()
        return redirect(index_depara)
