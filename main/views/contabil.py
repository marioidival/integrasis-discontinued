from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import RequestContext
from django.forms.models import inlineformset_factory
from main.views.index import index_depara

from main.models import Empresa, ContasLancaCont, HistoricoLancaCont, HistoricoLancaCont
from main.forms import ContaContabForm
from django.contrib.auth.decorators import login_required

#CONTABIL = DE-PARAS
@login_required()
def create_contabil(request):
    empresa_i = request.GET['empresa_id']

    empresa = Empresa.objects.get(pk=empresa_i)
    ContaContabFormset = inlineformset_factory(Empresa, ContasLancaCont,
            max_num=1, can_delete=False)
    formset = ContaContabFormset(request.POST or None)

    if request.method == "POST":
        if formset.is_valid():
            data = formset.cleaned_data
            for toba in data:
                toba['empresa'] = empresa
                ContasLancaCont.objects.create(**toba)
            return redirect(index_depara)
        else:
            return render_to_response('deparas/contabil/new.html',locals(),
            context_instance=RequestContext(request))


    return render_to_response('deparas/contabil/new.html',locals(),
            context_instance=RequestContext(request))

@login_required()
def edit_contabil(request, conta):
    obj = get_object_or_404(ContasLancaCont, pk=conta)
    if request.method == "POST":
        form = ContaContabForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            return redirect(index_depara)
        else:
            return render_to_response('deparas/contabil/edit.html', locals(),
                    context_instance=RequestContext(request))
    else:
        form = ContaContabForm(instance=obj)

    return render_to_response('deparas/contabil/edit.html', locals() ,
            context_instance=RequestContext(request))


def delete_contabil(request, conta):
    obj = get_object_or_404(ContasLancaCont, pk=conta)
    if obj:
        obj.delete()
        return redirect(index_depara)

#HISTOCIO = DEPARA

@login_required()
def create_historico(request):

    empresa_i = request.GET['empresa_id']

    empresa = Empresa.objects.get(pk=empresa_i)
    HistoricoLancaFormset = inlineformset_factory(Empresa, HistoricoLancaCont, max_num=1, can_delete=False)
    formset = HistoricoLancaFormset(request.POST or None)

    if request.method == "POST":
        if formset.is_valid():
            data = formset.cleaned_data
            for toba in data:
                #import ipdb; ipdb.set_trace()
                toba['empresa'] = empresa
                HistoricoLancaCont.objects.create(**toba)
            return redirect(index_depara)

    return render_to_response('deparas/contabil/new.html',locals(),
            context_instance=RequestContext(request))

@login_required()
def edit_historico(request, conta):
    obj = get_object_or_404(HistoricoLancaCont, pk=conta)
    if request.method == "POST":
        form = HistoricoLancaForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            return redirect(index_depara)
        else:
            return render_to_response('deparas/contabil/edit.html', locals(),
                    context_instance=RequestContext(request))
    else:
        form = HistoricoLancaForm(instance=obj)

    return render_to_response('deparas/contabil/edit.html', locals(),
                    context_instance=RequestContext(request))

@login_required()
def delete_historico(request, conta):
    obj = get_object_or_404(HistoricoLancaCont, pk=conta)
    if obj:
        obj.delete()
        return redirect(index_depara)

