from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.forms.models import inlineformset_factory
from django.template import RequestContext
from main.views.index import index_depara

from main.models import Empresa, ImpostoDePara
from main.forms import ImpostoDeParaForm
from django.contrib.auth.decorators import login_required

#TRIBUTOS - DE PARA

@login_required()
def create_tributos(request):
    empresa_i = request.GET['empresa_id']

    empresa = Empresa.objects.get(pk=empresa_i)
    ImpostosFormset = inlineformset_factory(Empresa, ImpostoDePara, max_num=1)
    formset = ImpostosFormset(request.POST or None)

    if request.method == "POST":
        if formset.is_valid():
            data = formset.cleaned_data
            for toba in data:
                #import ipdb; ipdb.set_trace()
                toba.pop('DELETE')
                toba['empresa'] = empresa
                ImpostoDePara.objects.create(**toba)
            return redirect(index_depara)
        else:
            pass

        return render_to_response('deparas/tributos/new.html', locals(),
                context_instance=RequestContext(request))


@login_required()
def edit_tributos(request, conta):
    obj = get_object_or_404(ImpostoDePara, pk=conta)
    if request.method == "POST":
        form = ImpostoDeParaForm(request.POST, instance=obj)
        if form.is_valid():
            form.save()
            return redirect(index_depara)
        else:
            return render_to_response('deparas/tributos/edit.html', locals(),
                    context_instance=RequestContext(request))
    else:
        form = ImpostoDeParaForm(instance=obj)

    return render_to_response('deparas/tributos/edit.html', locals(),
                    context_instance=RequestContext(request))

@login_required()
def delete_tributos(request, conta):
    obj = get_object_or_404(ImpostoDePara, pk=conta)
    if obj:
        obj.delete()
        return redirect(index_depara)

