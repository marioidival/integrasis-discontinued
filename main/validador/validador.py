# encoding: utf-8
from main.models import Layout_Padrao, ContasLancaCont, HistoricoLancaCont,\
            DuplicatasDeParaEntrada, DuplicatasDeParaSaida, DeParaFilial,\
            Empresa, ImpostoDePara, CodImpostoDP
from main.persistense.base import ConexaoQuestor, ChaveImposto
from check.settings import MEDIA_ROOT
from itertools import izip
import datetime
import csv
import re
import shutil
import codecs
import collections

def validator(file_instance):

    response = {}
    error_lines = {}
    good_lines = {}
    sort_dict = {}
    know_dict = {}

    pattern = [patterns for patterns in Layout_Padrao.objects.filter(
        layout__exact=file_instance.layout.nome).order_by('padrao__ordem')]

    current_file = codecs.open(file_instance.docfile.path, 'r', 'iso8859-1')
    generator_line = (lists.split(";") for lists in\
            current_file.readlines())
    current_file.close()

    for num, generator_list in enumerate(generator_line):

        for pattern_field, current_list_field in izip(pattern, generator_list):
            match_result = re.match(str(pattern_field),
                                    current_list_field.strip(),
                                    re.LOCALE)

            if match_result is None:
                if not num in error_lines:
                    if current_list_field == '':
                        msg = "Formato do campo invalido: Campo Vazio - "
                        error_lines[num] = [generator_list, msg]
                    else:
                        msg = "Formato do campo invalido: " + current_list_field
                        error_lines[num] = [generator_list, msg]

                else:
                    error_lines[num][-1] += current_list_field
            else:
                if not num in error_lines:
                    good_lines[num] = [generator_list, "Ok"]
                elif num in error_lines:
                    try:
                        del good_lines[num]
                    except KeyError:
                        pass
                else:
                    pass


        if error_lines:
            for key, values in error_lines.items():
                try:
                    response[key+1] = values
                except KeyError:
                    pass

        for key, values in good_lines.items():
            try:
                response[key+1] = values
            except KeyError:
                pass

        sort_dict = collections.OrderedDict(sorted(response.items()))
        know_dict['erradas'] = error_lines
        know_dict['corretas'] = good_lines

    return sort_dict, know_dict

def de_para(file_instance):

    current_file = codecs.open(file_instance.docfile.path, 'r', 'iso8859-1')
    generator_line = (lists.strip().split(";") for lists in\
            current_file.readlines())
    current_file.close()

    result = {}
    response = []
    nota_sequencia = {}
    exception_errors = {}

    # Montar os IF ELSE para os layouts:
    # 01, 02, 03, 04, 05, 06
    for num_lin, generator_list in enumerate(generator_line):

        line = generator_list
        #Contas Contabeis
        if file_instance.layout.nome == "01":
            cnpj_filial = line[0]
            cont_deb = line[2]
            cont_cre = line[3]
            historico = line[4]
            tmp = line[5].encode('latin-1')
            line[5] = tmp
            # DE-PARA Conta Debito

            try:
                result_filial = DeParaFilial.objects.get(
                    de__exact=cnpj_filial, empresa=file_instance.empresa)
            except DeParaFilial.DoesNotExist, e:
                msg_err = "Nenhum De Para Filial foi encontrado "\
                + cnpj_filial
                if num_lin in exception_errors:
                    value = str(exception_errors[num_lin]) + "--" + msg_err
                    exception_errors[num_lin] = value
                else:
                    exception_errors[num_lin] = msg_err
                    result_filial = None

            if cont_deb:
                try:
                    result_deb = ContasLancaCont.objects.get(de__exact=cont_deb,
                        empresa=file_instance.empresa)
                except ContasLancaCont.DoesNotExist, e:
                    msg_err = "Nenhum De Para de Lancamento Contabil foi"\
                            " Encontrado: %s" %cont_deb
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) +" - " + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err
                    result_deb = None

            # DE-PARA Conte Credito
            if cont_cre:
                try:
                    result_cre = ContasLancaCont.objects.get(de__exact=cont_cre,
                        empresa=file_instance.empresa)
                except ContasLancaCont.DoesNotExist, e:
                    msg_err = "Nenhum De Para de Lancamento Contabil foi"\
                            " Encontrado: %s" %cont_cre
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) +" - " + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err
                    result_cre = None

            # IF-ELSE do HistoricoLancaCont
            if historico:
                try:
                    result_histo = HistoricoLancaCont.objects.get(
                        de__exact=historico, empresa=file_instance.empresa)
                except HistoricoLancaCont.DoesNotExist, e:
                    msg_err = "Nenhum De Para de Historico Contabil foi"\
                            " Encontrado: %s" % historico
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) +" - " + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err

            if not exception_errors:
                line.remove(cnpj_filial)
                line.remove(cont_deb)
                line.remove(cont_cre)
                line.remove(historico)
                try:
                    line.append(result_deb.para)
                    result_deb = None
                except Exception:
                    line.append('')
                    result_deb = None
                try:
                    line.append(result_cre.para)
                    result_cre = None
                except Exception:
                    line.append('')
                    result_cre = None
                try:
                    line.append(result_histo.para)
                    result_histo = None
                except Exception:
                    line.append('')
                    
            else:
                try:
                    exception_errors[num_lin]
                except KeyError:
                    line.remove(cnpj_filial)
                    try:
                        line.append(result_deb.para)
                        result_deb = None
                    except Exception:
                        line.append('')
                        result_deb = None
                    try:
                        line.append(result_cre.para)
                        result_cre = None
                    except Exception:
                        line.append('')
                        result_cre = None
                    try:
                        line.append(result_histo.para)
                        result_histo = None
                    except Exception:
                        line.append('')
                else:
                    del(line)

            try:
                response.append(line)
            except Exception:
                pass
        # Duplicatas Entrada
        elif file_instance.layout.nome == "02":
            banco_caixa = line[11]

            if banco_caixa == "B":

                cnpj_filial = line[0]
                fornecedor = cpnj_format(line[1])
                line[1] = fornecedor
                nota = line[2]
                banco_ent = line[12]
                agencia_ent = line[13]
                contat = line[14]
                conta_ent = contat.rstrip('\r\n')

                remove_banco_ent = line[12]
                remove_agencia = line[13]

                codigo_filial = file_instance.empresa.cod_questor
                try:
                    instance_questor = ConexaoQuestor(
                    codigo_filial, nota, fornecedor
                    )
                except ValueError, e:
                    msg_err = "Formato de Nota invalido: %s" %nota
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err

                chaveduplicataent = dict_except(
                        instance_questor.buscar_duplicata,
                        Exception, exception_errors, num_lin
                        )

                sequencia = dict_except(
                        instance_questor.buscar_seq_pagamento,
                        Exception, exception_errors, num_lin
                        )

                try:
                    result_filial = DeParaFilial.objects.get(
                        de__exact=cnpj_filial, empresa=file_instance.empresa)
                except DeParaFilial.DoesNotExist, e:
                    msg_err = "Nenhum De Para Filial foi encontrado "\
                    + cnpj_filial
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err
                    result_filial = None

                try:
                    result_duplicatas = DuplicatasDeParaEntrada.objects.get(
                         banco__exact=banco_ent, agencia__exact=agencia_ent,
                         conta__exact=conta_ent)
                except DuplicatasDeParaEntrada.DoesNotExist, e:
                    msg_err = "Nenhum De Para de Entrada foi encontrado para:"\
                            " Banco " + banco_ent + " Agencia:" + agencia_ent+\
                            " Conta:" + conta_ent
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err

                    result_duplicatas = None

                chave = str(nota) + str(fornecedor)
                cont = 0

                if not chave in nota_sequencia:
                    cont += 1
                    try:
                        nota_sequencia[chave] = sequencia
                    except KeyError:
                        pass
                else:
                    if nota_sequencia[chave] is None:
                        pass
                    else:
                        nota_sequencia[chave] += 1
                        cont += 1
                        if cont > 9:
                            raise Exception("Sequencia por Importação"\
                                    " ultrapassou o limite permitido de 9")

                if not exception_errors:
                    line.remove(cnpj_filial)
                    line.remove(banco_caixa)
                    line.remove(remove_banco_ent)
                    line.remove(remove_agencia)
                    line.remove(contat)
                    line.append(result_duplicatas.tb)
                    line.append(result_filial.para)
                    line.append(chaveduplicataent.next())
                    line.append(nota_sequencia[chave])
                    line.append(codigo_filial)
                else:
                    try:
                        exception_errors[num_lin]
                    except KeyError:
                        line.remove(cnpj_filial)
                        line.remove(banco_caixa)
                        line.remove(remove_banco_ent)
                        line.remove(remove_agencia)
                        line.remove(contat)
                        line.append(result_duplicatas.tb)
                        line.append(result_filial.para)
                        line.append(chaveduplicataent.next())
                        line.append(nota_sequencia[chave])
                        line.append(codigo_filial)
                    else:
                        del(line)

            elif banco_caixa == "C":
                cnpj_filial = line[0]
                fornecedor = line[1]
                nota = line[2]
                banco_ent = line[12]
                agencia_ent = line[13]
                try:
                    contat = line[14]
                except IndexError:
                    pass

                instance_questor = ConexaoQuestor(
                    file_instance.empresa.cod_questor, nota, fornecedor
                                                )
                chaveduplicata = dict_except(
                        instance_questor.buscar_duplicata,
                        Exception, exception_errors, num_lin
                        )
                sequencia = dict_except(
                        instance_questor.buscar_seq_pagamento,
                        Exception, exception_errors, num_lin
                        )

                try:
                    result_filial = DeParaFilial.objects.get(
                        de__exact=cnpj_filial, empresa=file_instance.empresa)
                except DeParaFilial.DoesNotExist, e:
                    msg_err = "Nenhum De Para Filial foi encontrado: "\
                    + cnpj_filial
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err

                    result_filial = None

                chave = str(nota) + str(fornecedor)
                cont = 0

                if not chave in nota_sequencia:
                    cont += 1
                    try:
                        nota_sequencia[chave] = sequencia
                    except:
                        pass
                else:
                    if nota_sequencia[chave] is None:
                        pass
                    else:
                        nota_sequencia[chave] += 1
                        cont += 1
                        if cont > 9:
                            raise Exception("Sequencia por Importação"\
                                    " ultrapassou o limite permitido de 9")

                if not exception_errors:
                    line.remove(banco_caixa)
                    line.remove(cnpj_filial)
                    line.remove(banco_ent)
                    line.remove(agencia_ent)
                    try:
                        line.remove(contat)
                    except ValueError:
                        pass
                    line.append(483)
                    line.append(result_filial.para)
                    line.append(chaveduplicata.next())
                    try:
                        line.append(nota_sequencia[chave])
                    except KeyError:
                        pass
                    line.append(file_instance.empresa.cod_questor)
                else:
                    try:
                        exception_errors[num_lin]
                    except KeyError:
                        line.remove(banco_caixa)
                        line.remove(cnpj_filial)
                        line.remove(banco_ent)
                        line.remove(agencia_ent)
                        try:
                            line.remove(contat)
                        except ValueError:
                            pass
                        line.append(483)
                        line.append(result_filial.para)
                        line.append(chaveduplicata.next())
                        try:
                            line.append(nota_sequencia[chave])
                        except KeyError:
                            pass
                        line.append(file_instance.empresa.cod_questor)
                    else:
                        del(line)

            try:
                response.append(line)
            except Exception:
                pass
        # Duplicatas Saida
        elif file_instance.layout.nome == "03":
            banco_caixa = line[11]

            if banco_caixa == "B":

                cnpj_filial = line[0]
                fornecedor = line[1]
                nota = line[2]
                banco_ent = line[12]
                agencia_ent = line[13]
                contat = line[14]
                conta_ent = contat.rstrip('\r\n')

                remove_banco_ent = line[12]
                remove_agencia = line[13]

                codigo_filial = file_instance.empresa.cod_questor
                try:

                    instance_questor = ConexaoQuestor(
                    codigo_filial, nota, fornecedor
                    )
                except ValueError, e:
                    msg_err = "Formato de Nota invalido: %s" %nota
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err

                chaveduplicatasai = dict_except(
                        instance_questor.buscar_duplicata_sai,
                        Exception, exception_errors, num_lin
                        )

                sequencia = dict_except(
                        instance_questor.buscar_seq_pagamento_sai,
                        Exception, exception_errors, num_lin
                        )

                try:
                    result_filial = DeParaFilial.objects.get(
                        de__exact=cnpj_filial, empresa=file_instance.empresa
                    )
                except DeParaFilial.DoesNotExist, e:
                    msg_err = "Nenhum De Para Filial foi encontrado: "\
                    + cnpj_filial
                    if num_lin in exception_errors:
                        aux_var = exception_errors[num_lin]
                        value = str(aux_var) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err

                try:
                    result_duplicatas = DuplicatasDeParaSaida.objects.get(
                        banco__exact=banco_ent, agencia__exact=agencia_ent,
                        conta__exact=conta_ent
                    )
                except DuplicatasDeParaSaida.DoesNotExist, e:
                    msg_err = "Nenhum  De Para de Saida foi encontrado: Banco "\
                              + banco_ent + " Agencia:" + agencia_ent + "\
                            Conta:" + conta_ent
                    if num_lin in exception_errors:
                        aux_var = exception_errors[num_lin]
                        value = str(aux_var) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err

                    pass

                chave = str(nota) + str(fornecedor)
                cont = 0

                if not chave in nota_sequencia:
                    cont += 1
                    try:
                        nota_sequencia[chave] = sequencia
                    except:
                        pass
                else:
                    if nota_sequencia[chave] is None:
                        pass
                    else:
                        nota_sequencia[chave] += 1
                        cont += 1
                        if cont > 9:
                            raise Exception("Sequencia por Importação"\
                                    " ultrapassou o limite permitido de 9")

                if not exception_errors:
                    line.remove(cnpj_filial)
                    line.remove(banco_caixa)
                    line.remove(remove_banco_ent)
                    line.remove(remove_agencia)
                    line.remove(contat)
                    line.append(result_duplicatas.tb)
                    line.append(result_filial.para)
                    line.append(chaveduplicatasai)
                    line.append(nota_sequencia[chave])
                    line.append(codigo_filial)
                else:
                    try:
                        exception_errors[num_lin]
                    except KeyError:
                        line.remove(cnpj_filial)
                        line.remove(banco_caixa)
                        line.remove(remove_banco_ent)
                        line.remove(remove_agencia)
                        line.remove(contat)
                        line.append(result_duplicatas.tb)
                        line.append(result_filial.para)
                        line.append(chaveduplicatasai)
                        line.append(nota_sequencia[chave])
                        line.append(codigo_filial)
                    else:
                        del(line)
            elif banco_caixa == "C":
                cnpj_filial = line[0]
                fornecedor = line[1]
                nota = line[2]
                banco_ent = line[12]
                agencia_ent = line[13]
                try:
                    contat = line[14]
                except IndexError:
                    pass

                codigo_filial = file_instance.empresa.cod_questor
                try:
                    instance_questor = ConexaoQuestor(
                    codigo_filial, nota, fornecedor
                    )
                except ValueError, e:
                    msg_err = "Formato de Nota invalido: %s" %nota 
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err

                chaveduplicata = dict_except(
                        instance_questor.buscar_duplicata_sai,
                        Exception, exception_errors, num_lin
                        )

                sequencia = dict_except(
                        instance_questor.buscar_seq_pagamento_sai,
                        Exception, exception_errors, num_lin
                        )
                try:
                    result_filial = DeParaFilial.objects.get(
                        de__exact=cnpj_filial, empresa=file_instance.empresa)
                except DeParaFilial.DoesNotExist, e:
                    msg_err = "Nenhum De Para Filial foi encontrado: "\
                    + cnpj_filial
                    if num_lin in exception_errors:
                        aux_var = exception_errors[num_lin]
                        value = str(aux_var) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err

                chave = str(nota) + str(fornecedor)
                cont = 0

                if not exception_errors:
                    if not chave in nota_sequencia:
                        cont += 1
                        try:
                            nota_sequencia[chave] = sequencia
                        except:
                            pass
                    else:
                        value = nota_sequencia[chave]
                        value += 1
                        nota_sequencia[chave] = value
                        cont += 1
                        if cont > 9:
                            raise Exception("Sequencia por Importação ultrapassou"
                                            " o limite permitido de 9")

                    line.remove(banco_caixa)
                    line.remove(cnpj_filial)
                    line.remove(banco_ent)
                    line.remove(agencia_ent)
                    try:
                        line.remove(contat)
                    except ValueError:
                        pass
                    line.append(485)
                    line.append(result_filial.para)
                    line.append(chaveduplicata)
                    try:
                        line.append(nota_sequencia[chave])
                    except KeyError:
                        pass
                    line.append(file_instance.empresa.cod_questor)
                else:
                    try:
                        exception_errors[num_lin]
                    except KeyError:
                        if not chave in nota_sequencia:
                            cont += 1
                            try:
                                nota_sequencia[chave] = sequencia
                            except:
                                pass
                            else:
                                value = nota_sequencia[chave]
                                value += 1
                                nota_sequencia[chave] = value
                                cont += 1
                                if cont > 9:
                                    raise Exception("Sequencia por Importação ultrapassou"
                                                    " o limite permitido de 9")

                        line.remove(banco_caixa)
                        line.remove(cnpj_filial)
                        line.remove(banco_ent)
                        line.remove(agencia_ent)
                        try:
                            line.remove(contat)
                        except ValueError:
                            pass
                        line.append(485)
                        line.append(result_filial.para)
                        line.append(chaveduplicata)
                        try:
                            line.append(nota_sequencia[chave])
                        except KeyError:
                            pass
                        line.append(file_instance.empresa.cod_questor)
                    else:
                        del(line)

            try:
                response.append(line)
            except Exception:
                pass
        # Imposto Municipal
        elif file_instance.layout.nome == "04":
            banco_caixa = line[7]

            if banco_caixa == "B":

                cnpj_filial = line[0]
                data_apuracao = line[1]
                banco_ent = line[8]
                agencia_ent = line[9]
                contat = line[10]
                conta_ent = contat.rstrip('\r\n')

                remove_banco_ent = line[8]
                remove_agencia = line[9]

                codigo_filial = file_instance.empresa.cod_questor
                # OK
                try:
                    result_filial = DeParaFilial.objects.get(
                        de__exact=cnpj_filial, empresa=file_instance.empresa)
                except DeParaFilial.DoesNotExist, e:
                    msg_err = "Nenhum De Para Filial foi encontrado "\
                    + cnpj_filial
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err
                    result_filial = None
                # ???
                try:
                    result_duplicatas = ImpostoDePara.objects.get(
                         banco__exact=banco_ent, agencia__exact=agencia_ent,
                         conta__exact=conta_ent
                    )
                except ImpostoDePara.DoesNotExist, e:
                    msg_err = "Nenhum De Para foi encontrado para:"\
                            " Banco " + banco_ent + " Agencia:" + agencia_ent+\
                            " Conta:" + conta_ent
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err

                    result_duplicatas = None
                #OK
                instance_imp = ChaveImposto(
                    codigo_filial, data_apuracao
                )
                #OK
                chave_mun = dict_except(
                    instance_imp.chave_municipal,
                    Exception, exception_errors, num_lin
                )
                #OK
                seq_mun = dict_except(
                    instance_imp.seq_municipal,
                    Exception, exception_errors, num_lin
                )

                chave_dict = str(chave_mun.next())

                if not chave_dict in nota_sequencia:
                    try:
                        nota_sequencia[chave_dict] = seq_mun
                    except KeyError:
                        pass
                else:
                    if nota_sequencia[chave_dict] is None:
                        pass
                    else:
                        nota_sequencia[chave_dict] += 1

                if not exception_errors:
                    line.remove(cnpj_filial)
                    line.remove(banco_caixa)
                    line.remove(remove_banco_ent)
                    line.remove(remove_agencia)
                    line.remove(contat)
                    line.append(result_duplicatas.tb)
                    line.append(result_filial.para)
                    line.append(chave_mun.next())
                    line.append(nota_sequencia[chave_dict])
                    line.append(codigo_filial)
                else:
                    if num_lin in exception_errors:
                        pass
                    else:
                        exception_errors[num_lin] = 'Ok'
            elif banco_caixa == "C":
                cnpj_filial = line[0]
                data_apuracao = line[1]
                nota = line[2]
                banco_ent = line[9]
                agencia_ent = line[10]
                try:
                    contat = line[11]
                except IndexError:
                    pass

                instance_imp = ChaveImposto(
                    file_instance.empresa.cod_questor, data_apuracao
                )
                chave_mun = dict_except(
                        instance_imp.chave_municipal,
                        Exception, exception_errors, num_lin
                        )
                seq_mun = dict_except(
                        instance_imp.seq_municipal,
                        Exception, exception_errors, num_lin
                )
                # OK
                try:
                    result_filial = DeParaFilial.objects.get(
                        de__exact=cnpj_filial, empresa=file_instance.empresa)
                except DeParaFilial.DoesNotExist, e:
                    msg_err = "Nenhum De Para Filial foi encontrado: "\
                    + cnpj_filial
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err

                    result_filial = None

                chave_dict = str(chave_mun.next())
                #OK
                if not chave_dict in nota_sequencia:
                    try:
                        nota_sequencia[chave_dict] = seq_mun
                    except:
                        pass
                else:
                    if nota_sequencia[chave_dict] is None:
                        pass
                    else:
                        nota_sequencia[chave_dict] += 1

                if not exception_errors:
                    line.remove(banco_caixa)
                    line.remove(cnpj_filial)
                    line.remove(banco_ent)
                    line.remove(agencia_ent)
                    try:
                        line.remove(contat)
                    except ValueError:
                        pass
                    line.append(13)
                    line.append(result_filial.para)
                    line.append(chave_mun.next())
                    line.append(nota_sequencia[chave_dict])
                    line.append(file_instance.empresa.cod_questor)
                else:
                    if num_lin in exception_errors:
                        pass
                    else:
                        exception_errors[num_lin] = 'Ok'
            else:
                pass
            response.append(line)
        # Imposto Estadual
        elif file_instance.layout.nome == "05":
            banco_caixa = line[8]

            if banco_caixa == "B":

                cnpj_filial = line[0]
                impostos = line[1].encode('latin-1')
                data_apuracao = line[2]
                banco_ent = line[9]
                agencia_ent = line[10]
                contat = line[11]
                conta_ent = contat.rstrip('\r\n')

                remove_banco_ent = line[9]
                remove_agencia = line[10]

                codigo_filial = file_instance.empresa.cod_questor
                # OK
                try:
                    result_filial = DeParaFilial.objects.get(
                        de__exact=cnpj_filial, empresa=file_instance.empresa)
                except DeParaFilial.DoesNotExist, e:
                    msg_err = "Nenhum De Para Filial foi encontrado "\
                    + cnpj_filial
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err
                    result_filial = None
                # ??? - OK PARA ESTADUAL
                try:
                    result_depara_imposto = ImpostoDePara.objects.get(
                         banco__exact=banco_ent, agencia__exact=agencia_ent,
                         conta__exact=conta_ent, imposto=impostos
                    )
                except ImpostoDePara.DoesNotExist, e:
                    msg_err = "Nenhum De Para foi encontrado para:"\
                            " Banco: " + banco_ent + " Agencia:" + agencia_ent+\
                            " Conta: " + conta_ent + "Imposto: " + impostos
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err

                    result_duplicatas = None
                # CodImpostoDP
                try:
                    instance_cidp = CodImpostoDP.objects.get(
                        imposto=impostos
                    )
                except CodImpostoDP.DoesNotExist, e:
                    msg_err = "Nenhum Codigo Foi encontrado para " + impostos
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err

                    instance_cidp = None
                #OK
                instance_imp = ChaveImposto(
                    codigo_filial, data_apuracao,
                    instance_cidp.codigo, instance_cidp.variacao
                )
                #OK
                chave_est = dict_except(
                    instance_imp.chave_estadual,
                    Exception, exception_errors, num_lin
                )
                #OK
                seq_est = dict_except(
                    instance_imp.seq_estadual,
                    Exception, exception_errors, num_lin
                )

                chave_dict = str(chave_est.next())

                if not chave_dict in nota_sequencia:
                    try:
                        nota_sequencia[chave_dict] = seq_est
                    except KeyError:
                        pass
                else:
                    if nota_sequencia[chave_dict] is None:
                        pass
                    else:
                        nota_sequencia[chave_dict] += 1

                if not exception_errors:
                    line.remove(cnpj_filial)
                    line.remove(impostos)
                    line.remove(banco_caixa)
                    line.remove(remove_banco_ent)
                    line.remove(remove_agencia)
                    line.remove(contat)
                    line.append(result_depara_imposto.tb)
                    line.append(result_filial.para)
                    line.append(chave_est.next())
                    line.append(nota_sequencia[chave_dict])
                    line.append(codigo_filial)
                else:
                    if num_lin in exception_errors:
                        pass
                    else:
                        exception_errors[num_lin] = 'Ok'
            elif banco_caixa == "C":
                cnpj_filial = line[0]
                impostos = line[1].encode('latin-1')
                data_apuracao = line[2]
                banco_ent = line[9]
                agencia_ent = line[10]
                try:
                    contat = line[11]
                except IndexError:
                    pass
                #De Para Filial
                try:
                    result_filial = DeParaFilial.objects.get(
                        de__exact=cnpj_filial, empresa=file_instance.empresa
                    )
                except DeParaFilial.DoesNotExist, e:
                    msg_err = "Nenhum De Para Filial foi encontrado "\
                    + cnpj_filial
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err
                    result_filial = None
                # CodImpostoDP
                try:
                    instance_cidp = CodImpostoDP.objects.get(
                        imposto=impostos
                    )
                except CodImpostoDP.DoesNotExist, e:
                    msg_err = "Nenhum Codigo Foi encontrado para " + impostos
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err

                    instance_cidp = None
                #OK
                instance_imp = ChaveImposto(
                    codigo_filial, data_apuracao,
                    instance_cidp.codigo, instance_cidp.variacao
                )
                #OK
                chave_est = dict_except(
                    instance_imp.chave_estadual,
                    Exception, exception_errors, num_lin
                )
                #OK
                seq_est = dict_except(
                    instance_imp.seq_estadual,
                    Exception, exception_errors, num_lin
                )

                chave_dict = str(chave_est.next())
                #OK
                if not chave_dict in nota_sequencia:
                    try:
                        nota_sequencia[chave_dict] = seq_est
                    except:
                        pass
                else:
                    if nota_sequencia[chave_dict] is None:
                        pass
                    else:
                        nota_sequencia[chave_dict] += 1

                if not exception_errors:
                    line.remove(banco_caixa)
                    line.remove(impostos)
                    line.remove(cnpj_filial)
                    line.remove(banco_ent)
                    line.remove(agencia_ent)
                    try:
                        line.remove(contat)
                    except ValueError:
                        pass
                    line.append(instance_cidp.tb)
                    line.append(result_filial.para)
                    line.append(chave_est.next())
                    try:
                        line.append(nota_sequencia[chave_dict])
                    except KeyError:
                        pass
                    line.append(file_instance.empresa.cod_questor)
                else:
                    if num_lin in exception_errors:
                        pass
                    else:
                        exception_errors[num_lin] = 'Ok'
            else:
                pass
            response.append(line)
        # Imposto Federal
        elif file_instance.layout.nome == "06":
            banco_caixa = line[10]

            if banco_caixa == "B":

                cnpj_filial = line[0]
                impostos = line[1].encode('latin-1')
                fornecedor = line[2]
                nota = line[3]
                data_apuracao = line[4]
                banco_ent = line[11]
                agencia_ent = line[12]
                contat = line[13]
                conta_ent = contat.rstrip('\r\n')

                remove_banco_ent = line[11]
                remove_agencia = line[12]

                codigo_filial = file_instance.empresa.cod_questor
                # OK
                try:
                    result_filial = DeParaFilial.objects.get(
                        de__exact=cnpj_filial, empresa=file_instance.empresa)
                except DeParaFilial.DoesNotExist, e:
                    msg_err = "Nenhum De Para Filial foi encontrado "\
                    + cnpj_filial
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err
                    result_filial = None
                # ??? - OK PARA ESTADUAL
                try:
                    result_depara_imposto = ImpostoDePara.objects.get(
                         banco__exact=banco_ent, agencia__exact=agencia_ent,
                         conta__exact=conta_ent, imposto=impostos
                    )
                except ImpostoDePara.DoesNotExist, e:
                    msg_err = "Nenhum De Para foi encontrado para:"\
                            " Banco: " + banco_ent + " Agencia:" + agencia_ent+\
                            " Conta: " + conta_ent + "Imposto: " + impostos
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err

                    result_duplicatas = None
                # CodImpostoDP
                try:
                    instance_cidp = CodImpostoDP.objects.get(
                        imposto=impostos
                    )
                except CodImpostoDP.DoesNotExist, e:
                    msg_err = "Nenhum Codigo Foi encontrado para " + impostos
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err

                    instance_cidp = None

                if instance_cidp:
                    #OK
                    instance_imp = ChaveImposto(
                        codigo_filial, data_apuracao,
                        instance_cidp.codigo, instance_cidp.variacao,
                        fornecedor, instance_cidp.retido
                    )
                    #OK
                    # Is Retido?
                    if instance_cidp.retido == 'R':
                        chave_fed = dict_except(
                            instance_imp.chave_federal_retido,
                            Exception, exception_errors, num_lin
                        )
                    else:
                        chave_fed = dict_except(
                            instance_imp.chave_federal_n_retido,
                            Exception, exception_errors, num_lin
                        )
                    #OK
                    seq_fed = dict_except(
                        instance_imp.seq_federal,
                        Exception, exception_errors, num_lin
                    )

                    try:
                        chave_dict = str(chave_fed)

                        if not chave_dict in nota_sequencia:
                            try:
                                nota_sequencia[chave_dict] = seq_fed
                            except KeyError:
                                pass
                        else:
                            if nota_sequencia[chave_dict] is None:
                                pass
                            else:
                                nota_sequencia[chave_dict] += 1
                    except Exception:
                        pass

                else:
                    pass

                if not exception_errors:
                    line.remove(cnpj_filial)
                    line.remove(impostos)
                    try:
                        line.remove(fornecedor)
                    except ValueError:
                        pass
                    line.remove(nota)
                    line.remove(banco_caixa)
                    line.remove(remove_banco_ent)
                    line.remove(remove_agencia)
                    line.remove(contat)
                    line.append(result_depara_imposto.tb)
                    line.append(result_filial.para)
                    line.append(chave_fed)
                    line.append(nota_sequencia[chave_dict])
                    line.append(codigo_filial)
                else:
                    if num_lin in exception_errors:
                        pass
                    else:
                        exception_errors[num_lin] = 'Ok'
            elif banco_caixa == "C":

                cnpj_filial = line[0]
                impostos = line[1].encode('latin-1')
                fornecedor = line[2]
                nota = line[3]
                data_apuracao = line[4]
                banco_ent = line[11]
                agencia_ent = line[12]
                contat = line[13]
                try:
                    contat = line[13]
                except IndexError:
                    pass
                #De Para Filial
                codigo_filial = file_instance.empresa.cod_questor
                # OK
                try:
                    result_filial = DeParaFilial.objects.get(
                        de__exact=cnpj_filial, empresa=file_instance.empresa)
                except DeParaFilial.DoesNotExist, e:
                    msg_err = "Nenhum De Para Filial foi encontrado "\
                    + cnpj_filial
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err
                    result_filial = None
                # CodImpostoDP
                try:
                    instance_cidp = CodImpostoDP.objects.get(
                        imposto=impostos
                    )
                except CodImpostoDP.DoesNotExist, e:
                    msg_err = "Nenhum Codigo Foi encontrado para " + impostos
                    if num_lin in exception_errors:
                        value = str(exception_errors[num_lin]) + "--" + msg_err
                        exception_errors[num_lin] = value
                    else:
                        exception_errors[num_lin] = msg_err

                    instance_cidp = None

                if instance_cidp:
                    #OK
                    instance_imp = ChaveImposto(
                        codigo_filial, data_apuracao,
                        instance_cidp.codigo, instance_cidp.variacao,
                        fornecedor, instance_cidp.retido
                    )
                    #OK
                    # Eh Retido?
                    if instance_cidp.retido == 'R':
                        chave_fed = dict_except(
                            instance_imp.chave_federal_retido,
                            Exception, exception_errors, num_lin
                        )
                    else:
                        chave_fed = dict_except(
                            instance_imp.chave_federal_n_retido,
                            Exception, exception_errors, num_lin
                        )
                    #OK
                    seq_fed = dict_except(
                        instance_imp.seq_federal,
                        Exception, exception_errors, num_lin
                    )
                    try:
                        chave_dict = str(chave_fed)

                        if not chave_dict in nota_sequencia:
                            try:
                                nota_sequencia[chave_dict] = seq_fed
                            except KeyError:
                                pass
                        else:
                            if nota_sequencia[chave_dict] is None:
                                pass
                            else:
                                nota_sequencia[chave_dict] += 1
                    except Exception:
                        pass
                else:
                    pass

                if not exception_errors:
                    line.remove(banco_caixa)
                    line.remove(impostos)
                    try:
                        line.remove(fornecedor)
                    except ValueError:
                        pass
                    line.remove(nota)
                    line.remove(cnpj_filial)
                    line.remove(banco_ent)
                    line.remove(agencia_ent)
                    try:
                        line.remove(contat)
                    except ValueError:
                        pass
                    line.append(instance_cidp.tb)
                    line.append(result_filial.para)
                    line.append(chave_fed)
                    line.append(nota_sequencia[chave_dict])
                    line.append(file_instance.empresa.cod_questor)
                else:
                    if num_lin in exception_errors:
                        pass
                    else:
                        exception_errors[num_lin] = 'Ok'
            else:
                pass
            response.append(line)

    data = datetime.datetime.today()
    if exception_errors and response:
        data_nome = str(data.day)+ str(data.month) + str(data.year) +'_'+ str(data.minute) + str(data.second)
        name_file = file_instance.empresa.razao_social +'_'+ file_instance.layout.representacao+'_'+ data_nome+'_OK'
        nome_arquivo = create_file_csv(response, name_file)
        return exception_errors, nome_arquivo

    elif exception_errors:
        return exception_errors
    elif response:
        data_nome = str(data.day)+ str(data.month) + str(data.year) +'_'+ str(data.minute) + str(data.second)
        name_file = file_instance.empresa.razao_social +'_'+ file_instance.layout.representacao+'_'+ data_nome+'_OK'
        nome_arquivo = create_file_csv(response, name_file)
        return nome_arquivo

def dict_except(func, excp, dici, key):
    """
    Getting exceptions raising in functions
    Pass for an dictionary
    """
    try:
        result = func()
        return result
    except excp as msg:
        if key in dici:
            dici[key] += " -- %s" % str(msg)
        else:
            dici[key] = str(msg)

def create_file_csv(response_list, instance_file):
    with open(instance_file + '.csv', 'wb') as csvfile:

        inst = csv.writer(csvfile, delimiter=';', quotechar=' ',
                          quoting=csv.QUOTE_MINIMAL)
        inst.writerows(response_list)

    n_arquivo = csvfile.name
    try:
        shutil.move(n_arquivo, MEDIA_ROOT)
    except Exception:
        pass
    csvfile.close()
    return n_arquivo


def cpnj_format(cnpj):
    if len(cnpj) == 14:
        return "%s.%s.%s/%s-%s" % (cnpj[0:2], cnpj[2:5],
                cnpj[5:8], cnpj[8:12], cnpj[12:14] )
    elif len(cnpj) == 11:
        return "%s.%s.%s-%s" %( cnpj[0:3], cnpj[3:6],
                cnpj[6:9], cnpj[9:12])
    else:
        return cnpj
