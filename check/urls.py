# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

from main.views import contabil, fiscal, tributos, index

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'main.views.index.home', name='home'),
    url(r'^upload/', 'main.views.index.upload', name='upload'),
    url(r'^arquivos/', 'main.views.index.lista_arquivo', name='arquivos'),
    url(r'^search/', 'main.views.index.adv_search', name='search'),
    url(r'^search_depara/', 'main.views.index.adv_search_depara', name='search_depara'),

    (r'^grappelli/', include('grappelli.urls')),
    url(r'^detalhe/(?P<arq>\d+)', 'main.views.index.detalhe_arquivo',
        name='detalhe'),
    url(r'^detalhe-depara/(?P<arq>\d+)', 'main.views.index.detalhe_arquivo_depara',
        name='detalhedepara'),
    url(r'^validar/(?P<arq>\d+)', 'main.views.index.valida_arquivo',
        name='validar'),
    url(r'^deparas/(?P<arq>\d+)', 'main.views.index.use_de_para',
        name='de_para'),
    url(r'^donwload/(?P<arq>\d+)', 'main.views.index.download_file',
        name='downloadarquivo'),

    url(r'^deparas/$', 'main.views.index.index_depara', name='home_depara'),

        #Ajax
    url(r'^empresa_por_modulo/?$', 'main.views.index.empresa_por_modulo',
        name='empresapormodulo'),
    url(r'^ajax_index_depara/?$', 'main.views.index.ajax_index_depara',
        name='ajaxindex'),
    url(r'^empresas/?$', 'main.views.index.empresas', name='empresas_ajax'),

    url(r'^documentos/?$', 'main.views.index.documentos',
        name='documentos_ajax'),
    url(r'^doc_depara/?$', 'main.views.index.documentos_depara',
        name='depara_ajax'),


        #URLs De Para Contabil
    url(r'^contabil/nova-conta/?', 'main.views.contabil.create_contabil',
        name='create_contabil'),
    url(r'^contabil/(\d+)/editar/$', 'main.views.contabil.edit_contabil',
        name='edit_contabil'),
    url(r'^contabil/(\d+)/deletar/$', 'main.views.contabil.delete_contabil',
        name='delete_contabil'),

    url(r'^historico/novo-historico/?', 'main.views.contabil.create_historico',
        name='create_historico'),
    url(r'^historico/(\d+)/editar/$', 'main.views.contabil.edit_historico',
        name='edit_historico'),
    url(r'^historico/(\d+)/deletar/$', 'main.views.contabil.delete_historico',
        name='delete_historico'),

        #URLs De Para Fiscal

    url(r'^fiscal-entrada/nova-depara/?',
        'main.views.fiscal.create_fiscal_entrada', name='create_fiscal_entrada'),
    url(r'^fiscal-entrada/(\d+)/editar/$',
        'main.views.fiscal.edit_fiscal_entrada', name='edit_fiscal_entrada'),
    url(r'^fiscal-entrada/(\d+)/deletar/$',
        'main.views.fiscal.delete_fiscal_entrada', name='delete_fiscal_entrada'),

    url(r'^fiscal-filial/novo-depara/?',
        'main.views.fiscal.create_depara_filial',name='create_depara_filial'),
    url(r'^fiscal-filial/(\d+)/editar/$',
            'main.views.fiscal.edit_depara_filial',name='edit_depara_filial'),
    url(r'^fiscal-filial/(\d+)/deletar/$',
            'main.views.fiscal.delete_depara_filial',name='delete_depara_filial'),

    url(r'^fiscal-saida/nova-depara/?', 'main.views.fiscal.create_fiscal_saida',
            name='create_fiscal_saida'),
    url(r'^fiscal-saida/(\d+)/editar/$', 'main.views.fiscal.edit_fiscal_saida', 
            name='edit_fiscal_saida'),
    url(r'^fiscal-saida/(\d+)/deletar/$', 'main.views.fiscal.delete_fiscal_saida',
            name='delete_fiscal_saida'),

       #URLs De Para Tributos

    url(r'^tributo/nova-conta/?', 'main.views.tributos.create_tributos',
            name='create_tributos'),
    url(r'^tributo/(\d+)/editar/$', 'main.views.tributos.edit_tributos',
            name='edit_tributos'),
    url(r'^tributo/(\d+)/deletar/$', 'main.views.tributos.delete_tributos',
            name='delete_tributos'),

        #URLs de Login e LogOff
    url(r'^login/$', 'django.contrib.auth.views.login',
    {'template_name':'login.html'}, 'entrar'),
    url(r'^sair/$', 'django.contrib.auth.views.logout',
        {'next_page': '/login/'}),
    url(r'^admin/', include(admin.site.urls)),

)+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

